#!/bin/bash
gnuplot <<\EOF
reset

set terminal push  # save the current terminal setting
set terminal eps size 1.8,1.4 color solid enhanced font "Verdana,6"# change the terminal to pdf
set style fill  solid 1.00 #border lt 0
set bmargin 4
set grid ytics lc 9 


#set xtics scale 0 #rotate by -45 
set yrange [92:104]
#set xrange [-0.5:18]
set xtic 1 offset font "Verdana,6"
set ytics 1 font "Verdana,6"

set xlabel "Arrival rate of substreams S_1:S_2:S_3" font "Verdana,7"
set ylabel "Memoization (\%)" font "Verdana,7"
set arrow 1 from 16.75,-0.7 to 16.75,5.5 nohead lw 2 lt 0

set key center top  spacing 0.6 title "Sub-streams" width 1 horiz font "Verdana,6”
#set key center top  spacing 0.68 font ",4" title "  sub-streams   " width -9 horiz 
set style data histogram
set style fill solid 0.3 border

set style fill   solid 1.00 #border lt 0

set xtics offset 0.3 
set tics scale 0.5
set xtics nomirror
set ytics nomirror


set output "memoization.eps"

plot "output.dat" using 4:xticlabels(1) title "S_1" fs solid 0.8  lc "black",\
	 "" using 7:xtic(1) title "S_2" fs pattern 6 lc "black" ,\
	 "" using 10:xtic(1) title "S_3" fs solid 0.4 lc "black" ,\

set xtic 1 offset font "Verdana,6"

set output # set output to interactive mode
set terminal pop # restore the terminal


EOF
