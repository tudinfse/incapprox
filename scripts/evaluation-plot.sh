#!/bin/bash
gnuplot <<\EOF

reset
set terminal push
set terminal eps size 8.4,2.8 enhanced font "Verdana,12" color solid 

set font "Verdana,12"

#set out "mem-evaluation.eps"
set out "mem-evaluation.eps"
set multiplot layout 1,3


unset key
set key left title "Sub\-streams"
set key reverse
#set key center top spacing 0.88 title "arrival rate" width -1 horiz

set style line 1 lc rgb 'black' lt 4 lw 5 pt 7 ps 0.7
set style line 2 lc rgb '#696969' lt 3 lw 3 pt 2 ps 0.5
set style line 3 lc rgb '#363636' lt 6 lw 5 pt 9 ps 0.7
set style line 4 lc rgb '#696969' lt 3 lw 5 pt 5 ps 0.7

#set multiplot
set lmargin at screen 0.07
set rmargin at screen 0.31
set style data linespoints
set ytics 1000 font "Verdana,10" 
set xtics offset 0.5 font "Verdana,10" 
set tics scale 0.5 
set xtics nomirror 
set ytics nomirror 

#set tics format '\small $%h$'
set title '(a) Sample size '
set xlabel "Sample size (\%)" offset -1.0 font "Verdana,12"
set ylabel "Avg. \# memoized items" offset 2.0  font "Verdana,12"
set yrange [0:3500] noreverse nowriteback
set style line 12 lc rgb '#363636' lt 0 lw 3
set grid ytics ls 12
#set grid ytics lc 9 

#set out "mem-evaluation.pdf"

plot  "sampleSize.dat" using 2:xticlabels(1) title "S_1 " ls 1,\
	 "" using 3:xticlabels(1) title "S_2 " ls 3,\
	 "" using 4:xticlabels(1) title "S_3 " ls 4 


reset
set datafile missing '-'
set style data linespoints
set lmargin at screen 0.40
set rmargin at screen 0.64


set style line 1 lc rgb 'black' lt 3 lw 5 pt 7 ps 0.7
set style line 2 lc rgb '#696969' lt 3 lw 3 pt 2 ps 0.7
set style line 3 lc rgb '#363636' lt 6 lw 5 pt 9 ps 0.7
set style line 4 lc rgb '#696969' lt 3 lw 5 pt 5 ps 0.7


set ytics 100 font "Verdana,10"
set xtics offset 0.3  font "Verdana,10"
set tics scale 0.5
set xtics nomirror
set ytics nomirror

set style line 12 lc rgb '#363636' lt 0 lw 3 pt 3
set grid ytics ls 12


#set tics format '\small $%h$'
set key left title "Sub-streams"
set key reverse
#set key center top  spacing 0.58 title "sub-streams " width -1 horiz 

set title '(b) Slide interval '
set xlabel "Slide interval (\%)" offset -1.0 font "Verdana,12"
set ylabel " Avg. # memoized items" font "Verdana,12"
set yrange [100:600] noreverse nowriteback

plot  "slideInterval.dat" using 2:xticlabels(1) title "S_1 " ls 1,\
	 "" using 3:xticlabels(1) title "S_2 " ls 3,\
	 "" using 4:xticlabels(1) title "S_3 " ls 4


reset
set style fill   solid 1.00 border lt 0
set key left top
set key reverse
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set lmargin at screen 0.73
set rmargin at screen 0.99
set ytics 50  font "Verdana,10"
set xtics offset 0.3 font "Verdana,10" 
set tics scale 0.5
set xtics nomirror
set ytics nomirror
#set tics format '\small $%h$'
set title "(c) Window size"
set xlabel "{/Symbol D} change in window size" offset -1.5 font "Verdana,12"
set ylabel "No. samples" offset 1.0 font "Verdana,12"


set style data histogram
set style fill solid 0.3 border


set style line 12 lc rgb '#363636' lt 0 lw 3
set grid ytics ls 12


set yrange [ 900:1050] noreverse nowriteback

plot  "varyingWindow.dat" using 4:xticlabels(6) title "Memoized" fs solid 0.8  lc "black" ,\
	 "" using 5:xtic(6) title "  Sample size" fs solid 0.4 lc "black"


unset multiplot
set output
set term pop

EOF
