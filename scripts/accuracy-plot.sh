#!/bin/bash
gnuplot <<\EOF
reset

set terminal push  # save the current terminal setting
set terminal eps size 1.8, 1.6# change the terminal to pdf
set macros

YTICS = "set format y '%.0f'; set ylabel offset 1.2 'Accuracy loss (\%)' font 'Verdana,7'"
NOYTICS = "set format y ''; unset ylabel"
XTICS = "set xlabel offset 0,graph 0.08 'Sampling fraction (\%)' font 'Verdana,7'"
NOXTICS = "unset xlabel"


set style line 1 lc rgb 'black' lt 3 lw 3 pt 7 ps 0.5
set style line 2 lc rgb '#696969' lt 3 lw 3 pt 2 ps 0.4
set style line 3 lc rgb '#161616' lt 6 lw 3 pt 7 ps 0.4
set style line 4 lc rgb '#696969' lt 3 lw 3 pt 5 ps 0.4

set lmargin at screen 0.07
set rmargin at screen 0.31
set style data linespoints
set tics scale 0.5 

set lmargin 5.5
set rmargin 1.0
set tmargin 0.5
set bmargin 2.5

set out "accuracy-incapprox.eps"


set yrange [0:18] noreverse nowriteback
set xrange [-1:91]

set xtics (1, 5, 10, 40, 60, 80, 90)  out nomirror font "Verdana,6" offset 0, 0.5
set ytics 2 in nomirror font "Verdana,6"

unset key
set key top font "Verdana,6" spacing 1 vertical

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid ytics ls 12

@XTICS;@YTICS
plot 'error.dat' using 1:2 title 'Network monitoring' ls 3, \
	    '' using 1:3 title 'Twitter analytics' ls 4

set output # set output to interactive mode
set terminal pop # restore the terminal

set out
EOF
