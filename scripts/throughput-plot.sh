#!/bin/bash
gnuplot <<\EOF
reset

set terminal push  # save the current terminal setting
set terminal eps size 1.8, 1.6 # change the terminal to pdf
set macros

#set key top left
#key center top spacing 0.88 title "arrival rate" width -1 horiz 
#set key center top spacing 0.38  width -10 horiz font "Verdana,6"
unset key
set key center top font "Verdana,6" spacing 1 width -9 vertical
set style data histogram
set style fill solid 0.3 border

set yrange [100:900] noreverse nowriteback
set y2range [200:1800] noreverse nowriteback

set lmargin 4.5
set rmargin 5.5
set tmargin 0.5
set bmargin 2.2


set ytics 200 font 'Verdana,6' offset 0.8
set y2tics 200 font 'Verdana,6' offset -1
#set xtics scale 0
set style fill   solid 1.00 border lt 0
set xtics offset 0.3 
#set tics scale 0.5
set xtics nomirror
set ytics nomirror


set ylabel "#Flows ('000)" font 'Verdana,7'  offset 4
set y2label "#Tweets ('000)" font 'Verdana,7'  offset -4

set style line 12 lc rgb '#808080' lt 0 lw 1
set grid ytics ls 12

set xtics ("Inc" 0, "Approx" 1, "IncApprox" 2, "Spark Streaming" 3) font 'Verdana,7' rotate by -15 offset -0.5


set out "throughput-incapprox.eps"

plot  "throughput.dat" using 2 title "Network monitoring"  fs solid 0.8  lc "black" axes x1y1,\
      "throughput.dat" using 3 title "Twitter analytics"  fs solid 0.4 lc "black" axes x1y2


set out

EOF
