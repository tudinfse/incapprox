package com.incapprox.tests;

import com.incapprox.core.Stratified;
import com.incapprox.generateStreams.SimulateData;

public class TestCreateData extends Stratified {
	public static void main(String[] args) throws Throwable {

		/*
		 * auto-generated poisson streams with fixed arrival rate in the format
		 * "stratum:arrival_rate:time"
		 */

		SimulateData sm = new SimulateData();
		sm.create_poissonData();
		System.out
				.println("Poisson data stream saved in ./input/data_file.txt");
	}
}