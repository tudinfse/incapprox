package com.incapprox.tests;

import java.util.ArrayList;

import com.incapprox.core.Stratified;
import com.incapprox.core.Logging.Logging;

public class Test extends Stratified {
	public static void main(String[] args) throws Throwable {

		/*
		 * Input is auto-generated poisson streams in the format
		 * "stratum:arrival_rate:time" using SimulateData.create_poissonData()
		 */

		int window_type = 3;
		switch (window_type) {
		case 1: { // fixedsize Window, fixed slide interval

			Logging lg = new Logging();
			lg.initialize_fixedWindows();

			/*
			 * Process fixed size windows of data from auto-generated poisson
			 * streams
			 */
			int z = Stratified.process_fixedWindows();
			if (z == 1) {
				System.out
						.println("processing done, See final results in ./intermediate_results/logSummary.txt");
			}
			break;
		}
		case 2: { // varying window size

			@SuppressWarnings("rawtypes")
			ArrayList<ArrayList> t = new ArrayList<ArrayList>();

			/*
			 * Process varying size windows of data from auto-generated poisson
			 * streams
			 */
			t = Stratified.process_varyingWindows();

			Logging lg = new Logging();
			lg.initialize_varyingWindowLog(t);

			if (t != null) {
				System.out
						.println("processing done, See final results in ./intermediate_results/vary_log.txt");
			}
			break;
		}
		case 3: { // fluctuating arrival

			Logging lg = new Logging();
			lg.initialize_fluctuatingWindowLog();

			/*
			 * Process windows with fluctuating arrival rate of auto-generated
			 * poisson streams
			 */
			int j = Stratified.process_fluctuatingArrival();
			if (j == 1) {
				System.out
						.println("processing done, See final results in ./intermediate_results/fluctuating_arrival.txt");
			}
		}
			break;
		}
	}
}