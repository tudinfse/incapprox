package com.incapprox.generateStreams;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Fluctuating {

	/*
	 * Reads data streams with various arrival rates for generating a single
	 * fluctuating stream with varying arrival rates
	 */
	public ArrayList<File> readStream() throws Throwable {

		File s3_file = new File(
				"./input/fluctuating_arrivals/window_data/s3_rate_2.txt");

		File s1_file1 = new File("./input/fluctuating_arrivals/s1/s1_1.txt");
		File s2_file1 = new File("./input/fluctuating_arrivals/s2/s2_5.txt");

		File dataFile1 = new File(
				"./input/fluctuating_arrivals/window_data/data_1_5.txt");

		create_poissonData(1, 5, s1_file1, s2_file1, s3_file, dataFile1);

		File s1_file2 = new File("./input/fluctuating_arrivals/s1/s1_3.txt");
		File s2_file2 = new File("./input/fluctuating_arrivals/s2/s2_5.txt");

		File dataFile2 = new File(
				"./input/fluctuating_arrivals/window_data/data_3_5.txt");

		create_poissonData(3, 5, s1_file2, s2_file2, s3_file, dataFile2);

		File s1_file3 = new File("./input/fluctuating_arrivals/s1/s1_2.txt");
		File s2_file3 = new File("./input/fluctuating_arrivals/s2/s2_4.txt");

		File dataFile3 = new File(
				"./input/fluctuating_arrivals/window_data/data_2_4.txt");

		create_poissonData(2, 4, s1_file3, s2_file3, s3_file, dataFile3);

		File s1_file4 = new File("./input/fluctuating_arrivals/s1/s1_4.txt");
		File s2_file4 = new File("./input/fluctuating_arrivals/s2/s2_2.txt");

		File dataFile4 = new File(
				"./input/fluctuating_arrivals/window_data/data_4_2.txt");

		create_poissonData(4, 2, s1_file4, s2_file4, s3_file, dataFile4);

		File s1_file5 = new File("./input/fluctuating_arrivals/s1/s1_1.txt");
		File s2_file5 = new File("./input/fluctuating_arrivals/s2/s2_6.txt");

		File dataFile5 = new File(
				"./input/fluctuating_arrivals/window_data/data_1_6.txt");

		create_poissonData(1, 6, s1_file5, s2_file5, s3_file, dataFile5);

		ArrayList<File> dataFiles = new ArrayList<File>();
		dataFiles.add(dataFile1);// 1,5
		dataFiles.add(dataFile2);// 3,5
		dataFiles.add(dataFile3);// 2,4
		dataFiles.add(dataFile4);// 4,2
		dataFiles.add(dataFile5);// 1,6
		return dataFiles;

	}

	protected static void create_poissonData(int arr1, int arr2, File s1_file,
			File s2_file, File s3_file, File dataFile) throws Throwable {
		ArrayList<File> lst_input = new ArrayList<File>();
		lst_input.add(s1_file);
		lst_input.add(s2_file);
		lst_input.add(s3_file);

		ArrayList<String> lst_strata = new ArrayList<String>();
		lst_strata.add("s1");
		lst_strata.add("s2");
		lst_strata.add("s3");

		ArrayList<Integer> lst_arrival_rates = new ArrayList<Integer>();
		lst_arrival_rates.add(arr1);
		lst_arrival_rates.add(arr2);
		lst_arrival_rates.add(2);

		generate(lst_strata, lst_input, lst_arrival_rates, dataFile);
	}

	public static void generate(ArrayList<String> strata,
			ArrayList<File> file_poisson, ArrayList<Integer> arrival_rate,
			File data_file) throws Throwable {

		Scanner sc_s1 = new Scanner(new FileInputStream(file_poisson.get(0)));
		Scanner sc_s2 = new Scanner(new FileInputStream(file_poisson.get(1)));
		Scanner sc_s3 = new Scanner(new FileInputStream(file_poisson.get(2)));

		BufferedWriter output = null;
		output = new BufferedWriter(new FileWriter(data_file));

		// int seq = 0;
		int x, y, z;
		while (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()) {
			x = Integer.valueOf(sc_s1.next());
			while (x != 0) {
				output.write(strata.get(0).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				// seq++;
				x--;
			}
			y = Integer.valueOf(sc_s2.next());
			while (y != 0) {
				output.write(strata.get(1).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				// seq++;
				y--;
			}
			z = Integer.valueOf(sc_s3.next());
			while (z != 0) {
				output.write(strata.get(2).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				// seq++;
				z--;
			}
		}

		output.close();
		sc_s1.close();
		sc_s2.close();
		sc_s3.close();
	}

	/*
	 * Creates a fluctuating stream of data with 3 sub-streams s1,s2 and s3
	 */
	public void fluctuatingStream(ArrayList<File> files) throws Exception {

		Scanner sc_s1 = new Scanner(new FileInputStream(files.get(0)));
		Scanner sc_s2 = new Scanner(new FileInputStream(files.get(1)));
		Scanner sc_s3 = new Scanner(new FileInputStream(files.get(2)));
		Scanner sc_s4 = new Scanner(new FileInputStream(files.get(3)));
		Scanner sc_s5 = new Scanner(new FileInputStream(files.get(4)));

		File outputFile = new File(
				"./input/fluctuating_arrivals/window_data/dataStream.txt");

		BufferedWriter output = null;
		try {
			output = new BufferedWriter(new FileWriter(outputFile));
		} catch (IOException e) {
			e.printStackTrace();
		}

		int num = 1000;
		int x = num, limit = 0;

		while (limit < 1) {
			x = 1000;
			while (x > 0) {
				if (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()
						& sc_s4.hasNext() & sc_s5.hasNext()) {
					output.write(sc_s1.next());
					output.write("\n");
				}
				x--;
			}
			x = 1000;
			while (x > 0) {
				if (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()
						& sc_s4.hasNext() & sc_s5.hasNext()) {
					output.write(sc_s2.next());
					output.write("\n");
				}
				x--;
			}
			x = 1000;
			while (x > 0) {
				if (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()
						& sc_s4.hasNext() & sc_s5.hasNext()) {
					output.write(sc_s3.next());
					output.write("\n");
				}
				x--;
			}
			x = 1000;
			while (x > 0) {
				if (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()
						& sc_s4.hasNext() & sc_s5.hasNext()) {
					output.write(sc_s4.next());
					output.write("\n");
				}
				x--;
			}
			x = 1000;
			while (x > 0) {
				if (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()
						& sc_s4.hasNext() & sc_s5.hasNext()) {
					output.write(sc_s5.next());
					output.write("\n");
				}
				x--;
			}
			limit++;
		}

		sc_s1.close();
		sc_s2.close();
		sc_s3.close();
		sc_s4.close();
		sc_s5.close();
		output.close();

	}

}