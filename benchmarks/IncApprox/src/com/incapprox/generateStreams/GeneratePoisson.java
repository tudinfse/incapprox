package com.incapprox.generateStreams;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Scanner;

public class GeneratePoisson {
	/*
	 * Generates Poisson distributed data
	 * 
	 * @param strata : list of stratum
	 * 
	 * @param file_poisson : list of files to store poisson streams
	 * 
	 * @param arrival rate: rate of arrival of each sub-stream
	 * 
	 * @param data_file : output file to write full stream of data
	 */
	public void generate(ArrayList<String> strata,
			ArrayList<File> file_poisson, ArrayList<Integer> arrival_rate,
			File data_file) throws Throwable {

		Scanner sc_s1 = new Scanner(new FileInputStream(file_poisson.get(0)));
		Scanner sc_s2 = new Scanner(new FileInputStream(file_poisson.get(1)));
		Scanner sc_s3 = new Scanner(new FileInputStream(file_poisson.get(2)));

		BufferedWriter output = null;
		output = new BufferedWriter(new FileWriter(data_file));

		Time time;
		int x, y, z;

		while (sc_s1.hasNext() & sc_s2.hasNext() & sc_s3.hasNext()) {
			x = Integer.valueOf(sc_s1.next());
			while (x != 0) {
				output.write(strata.get(0).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				x--;
			}
			y = Integer.valueOf(sc_s2.next());
			while (y != 0) {
				output.write(strata.get(1).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				y--;
			}
			z = Integer.valueOf(sc_s3.next());
			while (z != 0) {
				output.write(strata.get(2).concat(":").concat("0").concat(":")
						.concat(String.valueOf(System.currentTimeMillis())));
				Thread.sleep(1);
				output.write("\n");
				z--;
			}
		}

		output.close();
		sc_s1.close();
		sc_s2.close();
		sc_s3.close();
	}
}