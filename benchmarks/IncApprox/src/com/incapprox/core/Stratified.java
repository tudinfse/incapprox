package com.incapprox.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class Stratified {
	public static int window_size;
	public static int slide;
	public static int reservoir_size;

	public static int wait_interval = 3;
	public static ArrayList<String> strataList = new ArrayList<String>();
	// 3 sub-reservoirs
	public static ArrayList<ItemObject> sub_r1 = new ArrayList<ItemObject>();
	public static ArrayList<ItemObject> sub_r2 = new ArrayList<ItemObject>();
	public static ArrayList<ItemObject> sub_r3 = new ArrayList<ItemObject>();

	public static int r1 = 0, r2 = 0, r3 = 0;// size of sub-reservoirs
	public static int count_s1, count_s2, count_s3;// count of items seen in
													// each strata
	public static ItemObject[] data;
	public static int k;
	public static int memoized;
	public static int file_end_flag = 0;

	public static ItemObject[] saved_Window = null;

	public static void init_window(int sizeOfWindow, int slide_interval,
			int sample_size) {
		window_size = sizeOfWindow;
		slide = slide_interval;
		reservoir_size = sample_size;
		data = new ItemObject[window_size];
		init_var();
	}

	private static void init_var() {
		count_s1 = 0;
		count_s2 = 0;
		count_s3 = 0;
		r1 = 0;
		r2 = 0;
		r3 = 0;
		wait_interval = 30;
	}

	public static int stratify_Sample(int Window_begin, ItemObject[] window_data) {
		int wait = 0;
		data = window_data;
		k = 0;
		init_var();
		memoized = 0;
		for (int i = Window_begin; i < window_size + Window_begin - 1; i = Window_begin
				+ k) {

			String t = data[0].getTimestamp();
			if (i == Window_begin && i != k) {
				memoize(t, Window_begin);
				memoized = 1;
			}
			// IMP: i and k are different, k is always 0 to windowsize
			// i is window_begin to window_end
			if (k <= reservoir_size) {
				if (data[k].strata.equals("s1")) {
					sub_r1.add(data[k]);
					r1++;
					k++;
					count_s1++;
				} else if (data[k].strata.equals("s2")) {
					sub_r2.add(data[k]);
					r2++;
					k++;
					count_s2++;
				} else if (data[k].strata.equals("s3")) {
					sub_r3.add(data[k]);
					r3++;
					k++;
					count_s3++;
				}
			} else {
				if (k >= window_size - 1) {
					return 0;
				}
				if (wait == wait_interval) {
					if (window_size > wait_interval) {
						compute_subReservoir();
						wait = 0;
					}
				} else {
					wait++;
					CRS();
				}
				if (k >= window_size - 1) {
					return 0;
				}
			}
		}
		return 0;
	}

	/*
	 * Calculates sub-reservoir sizes based on proportional allocation
	 */
	private static void compute_subReservoir() {
		int prev_r1 = r1;
		int prev_r2 = r2;
		int prev_r3 = r3;
		r1 = (reservoir_size * count_s1) / (k + 1);
		r2 = (reservoir_size * count_s2) / (k + 1);
		r3 = (reservoir_size * count_s3) / (k + 1);
		if (r1 != prev_r1) {
			ArrayList<Object> result = new ArrayList<Object>();
			result = ARS(sub_r1, r1 - prev_r1, "s1");
			if (result.get(0) != null) {
				sub_r1 = (ArrayList<ItemObject>) result.get(0);
				r1 = (int) result.get(1);
			}
		} else {
			CRS();
		}
		if (r2 != prev_r2) {
			ArrayList<Object> result = new ArrayList<Object>();
			result = ARS(sub_r2, r2 - prev_r2, "s2");
			if (result.get(0) != null) {
				sub_r2 = (ArrayList<ItemObject>) result.get(0);
				r2 = (int) result.get(1);
			}
		} else {
			CRS();
		}
		if (r3 != prev_r3) {
			ArrayList<Object> result = new ArrayList<Object>();
			result = ARS(sub_r3, r3 - prev_r3, "s3");
			if (result.get(0) != null) {
				sub_r3 = (ArrayList<ItemObject>) result.get(0);
				r3 = (int) result.get(1);
			}
		} else {
			CRS();
		}
	}

	/*
	 * Conventional Reservoir Sampling decides with random probability whether
	 * to accept the item
	 */
	private static void CRS() {

		if (k < window_size && k >= 1) {
			String type = data[k].getStrata();
			Random rn = new Random();
			int x = rn.nextInt(100000) + 1;
			if (x >= 50000 && type != null) {
				if (type.equals("s1") && sub_r1.size() > 0) {
					int r = sub_r1.size();
					int replace_elmt = rn.nextInt(r) + 0;
					sub_r1.set(replace_elmt, data[k]);
				}
				if (type.equals("s2") && sub_r2.size() > 0) {
					int r = sub_r2.size();
					int replace_elmt = rn.nextInt(r) + 0;
					sub_r2.set(replace_elmt, data[k]);
				}
				if (type.equals("s3") && sub_r3.size() > 0) {
					int r = sub_r3.size();
					int replace_elmt = rn.nextInt(r) + 0;
					sub_r3.set(replace_elmt, data[k]);
				}
			}
			if (k >= 0 && k < window_size) {
				update_var();
			}
		}
	}

	private static void update_var() {
		if (("s1").equals(data[k].getStrata())) {
			count_s1++;
		} else if (("s2").equals(data[k].getStrata())) {
			count_s2++;
		} else if (("s3").equals(data[k].getStrata())) {
			count_s3++;
		}
		k++;
	}

	/*
	 * Adaptive Reservoir Sampling adapts reservoir size based on sub-stream
	 * arrival rate
	 */
	private static ArrayList<Object> ARS(ArrayList<ItemObject> sub_r,
			int difference, String type) {
		if (difference > 0) {
			for (int p = 0; difference != 0 && k < window_size && k >= 1; p++) {
				if (type.equals(data[k].getStrata())) {
					sub_r.add(data[k]);
					difference--;
					update_var();
				} else {
					p--;
					CRS();
				}
			}

		} else if (difference < 0) {
			// Randomly evict (difference) number of elements from sub-reservoir
			Collections.shuffle(sub_r);
			for (int p = 0; p < (0 - difference) && sub_r.size() > 0; p++) {
				sub_r.remove(0);
			}
		}
		ArrayList<Object> res_and_size = new ArrayList<Object>();
		res_and_size.add(sub_r);
		res_and_size.add(sub_r.size());
		return res_and_size;
	}

	private static void write_samples() {
		BufferedWriter output1, output2 = null;
		BufferedWriter output3 = null;
		try {
			output1 = new BufferedWriter(new FileWriter(
					"./intermediate_results/s1_samples.txt"));
			int i = 0;
			while (i < sub_r1.size()) {
				output1.write(sub_r1.get(i).get());
				output1.write("\n");
				i++;
			}
			output2 = new BufferedWriter(new FileWriter(
					"./intermediate_results/s2_samples.txt"));
			i = 0;
			while (i < sub_r2.size()) {
				output2.write(sub_r2.get(i).get());
				output2.write("\n");
				i++;
			}
			output3 = new BufferedWriter(new FileWriter(
					"./intermediate_results/s3_samples.txt"));
			i = 0;
			while (i < sub_r3.size()) {
				output3.write(sub_r3.get(i).get());
				output3.write("\n");
				i++;
			}
			output1.close();
			output2.close();
			output3.close();
		} catch (IOException e) {
			System.out.println("Cannot read/write samples data");
		}

	}

	/*
	 * Variables used for finding average
	 */
	public static int s1_sum = 0, s2_sum = 0, s3_sum = 0;

	/*
	 * Memoizes the items from previous window
	 */
	private static int memoize(String timeStamp, int window_begin) {
		String x = null;
		BufferedWriter logSummary = null;
		BufferedWriter memoised_writer_s1 = null, memoised_writer_s2 = null, memoised_writer_s3 = null;
		try {
			int i = 0, j = 0, l = 0;
			int mem_s1 = 0, mem_s2 = 0, mem_s3 = 0;
			memoised_writer_s1 = new BufferedWriter(new FileWriter(
					"./intermediate_results/memoized_s1.txt"));
			memoised_writer_s2 = new BufferedWriter(new FileWriter(
					"./intermediate_results/memoized_s2.txt"));
			memoised_writer_s3 = new BufferedWriter(new FileWriter(
					"./intermediate_results/memoized_s3.txt"));
			logSummary = new BufferedWriter(new FileWriter(
					"./intermediate_results/logSummary.txt", true));

			if (sub_r1 != null) {
				i = 0;
				x = null;
				while (i < sub_r1.size()) {
					x = sub_r1.get(i).getTimestamp();

					if (Long.parseLong(timeStamp) <= Long.parseLong(x)) {
						memoised_writer_s1.write(sub_r1.get(i).get());
						mem_s1++;
						memoised_writer_s1.write("\n");
					}
					i++;
				}
			}
			if (sub_r2 != null) {
				j = 0;
				x = null;
				while (j < sub_r2.size()) {
					x = sub_r2.get(j).getTimestamp();
					if (Long.parseLong(timeStamp) <= Long.parseLong(x)) {
						memoised_writer_s2.write(sub_r2.get(j).get());
						mem_s2++;
						memoised_writer_s2.write("\n");
					}
					j++;
				}
			}
			if (sub_r3 != null) {
				l = 0;
				x = null;
				while (l < sub_r3.size()) {
					x = sub_r3.get(l).getTimestamp();
					if (Long.parseLong(timeStamp) <= Long.parseLong(x)) {
						memoised_writer_s3.write(sub_r3.get(l).get());
						mem_s3++;
						memoised_writer_s3.write("\n");
					}
					l++;
				}
			}

			logSummary.write("\n");

			logSummary.write((mem_s1) + "\t" + mem_s2 + "\t" + mem_s3 + "\t"
					+ (mem_s1 + mem_s2 + mem_s3));

			sub_r1.clear();
			sub_r2.clear();
			sub_r3.clear();
			logSummary.close();
			memoised_writer_s1.close();
			memoised_writer_s2.close();
			memoised_writer_s3.close();

		} catch (Exception e) {
			System.out.println("Cannot read/write memoized items");
		}
		return 0;
	}

	/*
	 * Incrementally updates the sub-reservoir contents with memoized items
	 */
	private static void incremental_update() throws Exception {

		BufferedReader mem_s1 = new BufferedReader(new FileReader(
				"./intermediate_results/memoized_s1.txt"));
		BufferedReader mem_s2 = new BufferedReader(new FileReader(
				"./intermediate_results/memoized_s2.txt"));
		BufferedReader mem_s3 = new BufferedReader(new FileReader(
				"./intermediate_results/memoized_s3.txt"));

		BufferedReader sample_s1 = new BufferedReader(new FileReader(
				"./intermediate_results/s1_samples.txt"));
		BufferedReader sample_s2 = new BufferedReader(new FileReader(
				"./intermediate_results/s2_samples.txt"));
		BufferedReader sample_s3 = new BufferedReader(new FileReader(
				"./intermediate_results/s3_samples.txt"));

		HashSet<ItemObject> hs_s1 = new HashSet<ItemObject>();
		String s1 = null;
		while ((s1 = sample_s1.readLine()) != null) {
			ItemObject t1 = new ItemObject();
			t1.set(s1);
			hs_s1.add(t1);
		}
		HashSet<ItemObject> hs_s2 = new HashSet<ItemObject>();
		String s2 = null;
		while ((s2 = sample_s2.readLine()) != null) {
			ItemObject t2 = new ItemObject();
			t2.set(s2);
			hs_s2.add(t2);
		}
		HashSet<ItemObject> hs_s3 = new HashSet<ItemObject>();
		String s3 = null;
		while ((s3 = sample_s3.readLine()) != null) {
			ItemObject t3 = new ItemObject();
			t3.set(s3);
			hs_s3.add(t3);
		}

		String memLine = null;
		int sample1_size = hs_s1.size();
		HashSet<ItemObject> hs_inc1 = new HashSet<ItemObject>();
		int flag1 = 0, i = 0;

		for (i = 0; i < sample1_size && flag1 == 0; i++) {
			memLine = mem_s1.readLine();
			ItemObject obj = new ItemObject();
			if (memLine != null) {
				obj.set(memLine);
				hs_inc1.add(obj);
			} else {
				flag1 = 1;
			}
		}
		// i-1 needed, as previous for loop increments 1 before exiting
		if (i - 1 < sample1_size) {
			Iterator<ItemObject> it1 = hs_s1.iterator();
			while (hs_inc1.size() < sample1_size) {
				ItemObject test = new ItemObject();
				test = it1.next();
				hs_inc1.add(test);
			}
		}

		memLine = null;
		int sample2_size = hs_s2.size();
		HashSet<ItemObject> hs_inc2 = new HashSet<ItemObject>();
		flag1 = 0;
		i = 0;
		for (i = 0; i < sample2_size && flag1 == 0; i++) {
			memLine = mem_s2.readLine();
			ItemObject obj = new ItemObject();
			if (memLine != null) {
				obj.set(memLine);
				hs_inc2.add(obj);
			} else {
				flag1 = 1;
			}
		}
		if (i - 1 < sample2_size) {
			Iterator<ItemObject> it2 = hs_s2.iterator();
			while (hs_inc2.size() < sample2_size) {
				ItemObject test = new ItemObject();
				test = it2.next();
				hs_inc2.add(test);
			}
		}

		memLine = null;
		int sample3_size = hs_s3.size();
		HashSet<ItemObject> hs_inc3 = new HashSet<ItemObject>();
		flag1 = 0;
		i = 0;

		for (i = 0; i < sample3_size && flag1 == 0; i++) {
			memLine = mem_s3.readLine();
			ItemObject obj = new ItemObject();
			if (memLine != null) {
				obj.set(memLine);
				hs_inc3.add(obj);
			} else {
				flag1 = 1;
			}
		}
		if (i - 1 < sample3_size) {
			Iterator<ItemObject> it3 = hs_s3.iterator();
			while (hs_inc3.size() < sample3_size) {
				ItemObject test = new ItemObject();
				test = it3.next();
				hs_inc3.add(test);
			}
		}

		mem_s1.close();
		mem_s2.close();
		mem_s3.close();
		sample_s1.close();
		sample_s2.close();
		sample_s3.close();
		write_inc(hs_inc1, hs_inc2, hs_inc3);
		write_count_log(hs_inc1, hs_inc2, hs_inc3);
		update_sub_reservoirs();

	}

	private static void extracted(String x) throws Exception {
		throw new Exception(x);
	}

	/*
	 * Write incrementally updated samples i.e, biased samples
	 */
	private static void write_inc(HashSet<ItemObject> hs_inc1,
			HashSet<ItemObject> hs_inc2, HashSet<ItemObject> hs_inc3)
			throws IOException {

		BufferedWriter inc_s1 = new BufferedWriter(new FileWriter(
				"./intermediate_results/inc_s1_samples.txt"));
		BufferedWriter inc_s2 = new BufferedWriter(new FileWriter(
				"./intermediate_results/inc_s2_samples.txt"));

		BufferedWriter inc_s3 = new BufferedWriter(new FileWriter(
				"./intermediate_results/inc_s3_samples.txt"));

		Iterator<ItemObject> iter1 = hs_inc1.iterator();
		Iterator<ItemObject> iter2 = hs_inc2.iterator();
		Iterator<ItemObject> iter3 = hs_inc3.iterator();

		while (iter1.hasNext()) {
			inc_s1.write(iter1.next().get());
			inc_s1.write("\n");
		}
		while (iter2.hasNext()) {
			inc_s2.write(iter2.next().get());
			inc_s2.write("\n");
		}
		while (iter3.hasNext()) {
			inc_s3.write(iter3.next().get());
			inc_s3.write("\n");
		}
		inc_s1.close();
		inc_s2.close();
		inc_s3.close();

	}

	/*
	 * Do biased sampling by updating sub-reservoir contents with memoized items
	 */
	private static void update_sub_reservoirs() throws FileNotFoundException {
		BufferedReader sample_r1 = new BufferedReader(new FileReader(
				"./intermediate_results/inc_s1_samples.txt"));
		BufferedReader sample_r2 = new BufferedReader(new FileReader(
				"./intermediate_results/inc_s2_samples.txt"));

		BufferedReader sample_r3 = new BufferedReader(new FileReader(
				"./intermediate_results/inc_s3_samples.txt"));

		sub_r1.clear();
		sub_r2.clear();
		sub_r3.clear();

		String sampleLine = null;
		try {
			int i = 0;
			while ((sampleLine = sample_r1.readLine()) != null) {
				ItemObject temp = new ItemObject();
				temp.set(sampleLine);
				sub_r1.add(temp);
				i++;
			}
			i = 0;
			while ((sampleLine = sample_r2.readLine()) != null) {
				ItemObject temp = new ItemObject();
				temp.set(sampleLine);
				sub_r2.add(temp);
				i++;
			}
			i = 0;
			while ((sampleLine = sample_r3.readLine()) != null) {
				ItemObject temp = new ItemObject();
				temp.set(sampleLine);
				sub_r3.add(temp);
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/*
	 * Read streaming data
	 */
	private static ItemObject[] readWindow(String file, int currWindow_begin) {
		ItemObject[] window = new ItemObject[window_size];
		for (int i = 0; i != window_size; i++) {
			window[i] = new ItemObject(null, 0, null);
		}
		File data_file = new File(file);
		try {
			Scanner reader = new Scanner(new BufferedReader(new FileReader(
					data_file)));
			int j = 0;
			while (j < currWindow_begin & reader.hasNext()) {
				reader.next();
				j++;
			}
			int i = 0;
			while (i < window_size) {
				if (reader.hasNext()) {
					window[i].set(reader.next());
				} else {
					file_end_flag = 1;
					window_size = i + 1;
				}
				i++;
			}
			reader.close();
			return window;
		} catch (FileNotFoundException e) {
			System.out
					.println("Cannot read data file for Sliding window/reader problem");
		}
		return window;
	}

	/*
	 * Process fixed size windows from auto-generated poisson streams
	 */
	protected static int process_fixedWindows() throws Exception {
		ItemObject[] window_data = null;
		int prevWindow_begin = 0, prevWindow_end = 0;
		int currWindow_begin = 0, currWindow_end = 0;
		currWindow_begin = 0;
		currWindow_end = window_size;

		file_end_flag = 0;
		for (int m = 0;; m++) {

			init_window(10000, 800, 1000);
			window_data = readWindow("./input/data_file.txt", currWindow_begin);
			stratify_Sample(currWindow_begin, window_data);
			write_samples();

			if (m != 0) {
				incremental_update();
			}

			if (file_end_flag == 1) {
				return 1;
			}
			prevWindow_begin = currWindow_begin;
			prevWindow_end = currWindow_end;
			currWindow_begin = prevWindow_begin + slide;
			currWindow_end = prevWindow_begin + slide + window_size - 1;

		}

		// return 0;
	}

	/*
	 * Write count of items for later processing
	 */
	private static void write_count_log(HashSet<ItemObject> hs_inc1,
			HashSet<ItemObject> hs_inc2, HashSet<ItemObject> hs_inc3)
			throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(
				"./intermediate_results/count_log.txt", true));

		int x = 0, y = 0, z = 0, c1 = 0, c2 = 0, c3 = 0;

		while (x < sub_r1.size()) {
			c1 += sub_r1.get(x).getDataItem();
			x++;
		}
		while (y < sub_r2.size()) {
			c2 += sub_r2.get(y).getDataItem();
			y++;
		}
		while (z < sub_r3.size()) {
			c3 += sub_r3.get(z).getDataItem();
			z++;
		}

		x = 0;
		y = 0;
		z = 0;
		int inc_c1 = 0, inc_c2 = 0, inc_c3 = 0;

		Iterator<ItemObject> iter1 = hs_inc1.iterator();
		Iterator<ItemObject> iter2 = hs_inc2.iterator();
		Iterator<ItemObject> iter3 = hs_inc3.iterator();

		while (iter1.hasNext()) {
			inc_c1 += iter1.next().getDataItem();
		}
		while (iter2.hasNext()) {
			inc_c2 += iter2.next().getDataItem();
		}
		while (iter3.hasNext()) {
			inc_c3 += iter3.next().getDataItem();
		}

		bw.write(String.valueOf(c1).concat(" ").concat(String.valueOf(c2))
				.concat(" ").concat(String.valueOf(c3)).concat(" ")
				.concat(String.valueOf(inc_c1)).concat(" ")
				.concat(String.valueOf(inc_c2)).concat(" ")
				.concat(String.valueOf(inc_c3)).concat(" ")
				.concat(String.valueOf(sub_r1.size())).concat(" ")
				.concat(String.valueOf(sub_r2.size())).concat(" ")
				.concat(String.valueOf(sub_r3.size())).concat(" ")
				.concat(String.valueOf(count_s1)).concat(" ")
				.concat(String.valueOf(count_s2)).concat(" ")
				.concat(String.valueOf(count_s3)).concat(" "));
		bw.write("\n");
		bw.close();
	}

	/*
	 * Process windows with fluctuating arrival rate of auto-generated poisson
	 * streams
	 */
	protected static int process_fluctuatingArrival() throws Throwable {
		ItemObject[] window_data = null;
		int prevWindow_begin = 0, prevWindow_end = 0;
		int currWindow_begin = 0, currWindow_end = 0;
		currWindow_begin = 0;
		currWindow_end = window_size;

		file_end_flag = 0;

		for (int m = 1;; m++) {

			// Data is already generated using the below commented code !
			// Fluctuating fluctuating = new Fluctuating();
			// ArrayList<File> sub_files = new ArrayList<File>();
			// sub_files = fluctuating.readStream();
			// fluctuating.fluctuatingStream(sub_files);

			init_window(1000, 10, 500);
			window_data = readWindow(
					"./input/fluctuating_arrivals/window_data/dataStream.txt",
					currWindow_begin);
			stratify_Sample(currWindow_begin, window_data);
			write_samples();
			if (m != 1) {
				incremental_update();
				write_final_log();
			}

			if (file_end_flag == 1) {
				return 1;
			}
			prevWindow_begin = currWindow_begin;
			prevWindow_end = currWindow_end;
			currWindow_begin = prevWindow_begin + slide;
			currWindow_end = prevWindow_begin + slide + window_size - 1;
			// System.out.println("new window begin and end:" + currWindow_begin
			// + "and" + currWindow_end);
			System.out.println("#window" + m);

		}

		// return 0;
	}

	private static void write_final_log() throws Exception {
		BufferedWriter fluctuating_log = new BufferedWriter(new FileWriter(
				"./intermediate_results/fluctuating_arrival.txt", true));

		BufferedReader reader = new BufferedReader(new FileReader(
				"./intermediate_results/logSummary.txt"));

		int i = 0;
		String sCurrentLine = "";
		String lastLine = "";
		String finalString = "";
		int check = 0;

		while ((sCurrentLine = reader.readLine()) != null) {
			lastLine = sCurrentLine;
		}

		String[] str = new String[3];
		str = lastLine.split("\t");
		String s1_per, s2_per, s3_per;
		if (!str.equals(null)) {
			s1_per = String.valueOf(((Integer.valueOf(str[0])) * 100)
					/ sub_r1.size());
			s2_per = String.valueOf(((Integer.valueOf(str[1])) * 100)
					/ sub_r2.size());
			s3_per = String.valueOf(((Integer.valueOf(str[2])) * 100)
					/ sub_r3.size());

			finalString = str[0].concat("\t")
					.concat(String.valueOf(sub_r1.size())).concat("\t")
					.concat(s1_per).concat("\t").concat(str[1]).concat("\t")
					.concat(String.valueOf(sub_r2.size())).concat("\t")
					.concat(s2_per).concat("\t").concat(str[2]).concat("\t")
					.concat(String.valueOf(sub_r3.size())).concat("\t")
					.concat(s3_per);

			fluctuating_log.write(finalString);
			fluctuating_log.write("\n");
			check = 1;
		} else {
			System.out.println("Something wrong at writeFinalLog()");
		}
		fluctuating_log.close();
		reader.close();

	}

	@SuppressWarnings("rawtypes")
	/*
	 * Processing varying size windows
	 */
	protected static ArrayList<ArrayList> process_varyingWindows()
			throws Exception {
		// Variation for each sub-sequent windows
		ArrayList<Integer> variation = new ArrayList<Integer>();
		variation.add(0);
		variation.add(+100);
		variation.add(-200);
		variation.add(+300);
		variation.add(-100);
		variation.add(+200);
		variation.add(-300);

		ItemObject[] window_data = null;
		int prevWindow_begin = 0, prevWindow_end = 0;
		int currWindow_begin = 0, currWindow_end = 0;
		currWindow_begin = 0;
		currWindow_end = window_size;

		file_end_flag = 0;
		int varying_size = 10000;
		int slide = 0, s_size = 0, index = 0;
		int l = variation.size();
		ArrayList<Integer> varying_sample = new ArrayList<Integer>();

		for (int m = 0; m < 9 && l > 0; m++, index++, l--) {

			varying_size += variation.get(index);

			slide = (int) (0.01 * varying_size);
			s_size = (int) (0.1 * varying_size);
			varying_sample.add(s_size);

			init_window(varying_size, slide, s_size);
			window_data = readWindow("./input/data_file.txt", currWindow_begin);
			stratify_Sample(currWindow_begin, window_data);
			write_samples();
			if (m != 0) {
				incremental_update();
			}

			prevWindow_begin = currWindow_begin;
			prevWindow_end = currWindow_end;
			currWindow_begin = prevWindow_begin + slide;
			currWindow_end = prevWindow_begin + slide + window_size - 1;

		}

		int k = 0;
		k = 0;
		while (k != variation.size()) {
			k++;
		}
		k = 0;
		while (k != varying_sample.size()) {
			k++;
		}

		@SuppressWarnings("rawtypes")
		ArrayList<ArrayList> t = new ArrayList<ArrayList>();
		t.add(varying_sample);
		t.add(variation);
		return t;
	}

}