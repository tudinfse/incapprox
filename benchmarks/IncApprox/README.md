# README #

*Input: Synthetic poisson stream with 3 substreams, each of different arrival rates

*Input is of the form sub-stream:value:time

*Output: Output is stored in log files, and terms s1 represent 1st sub-stream, s2 represents 2nd substream etc.

### How to run? ###

Test.java shows an example of using 3 different use-cases in the program, namely:

* Processing with fixed sized sliding windows

* Processing with variable sized sliding windows

* Processing with sub-streams of fluctuating arrival rates

### Output ###
./output :All final summary of outputs is in the output folder. It is used for plotting the various evaluation results based on slide interval, sample size, memoization and fluctuating arrival rate

./intermediate results: All the intermediate results for memoization, incremental computation, as well as the complete results of program(3 usecases) are stored here.
Only the final summary is in ./output folder

