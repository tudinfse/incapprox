import java.util.HashMap
import org.apache.kafka.clients.producer.{ProducerConfig, KafkaProducer, ProducerRecord}

import kafka.serializer.StringDecoder

import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

import org.apache.commons.math.distribution.TDistribution
import org.apache.commons.math.distribution.TDistributionImpl

import org.apache.log4j.Logger
import org.apache.log4j.Level

//import com.redis.RedisClient
//import com.redis._

object Inc {


  def getHashtagStatistic(message: String): (String, Double) = {
    val tweet = message.toLowerCase()
    if (tweet.length > 0) {

      if ((tweet contains "nike") && (tweet contains "adidas")) {
        println("adidas+nike")
        return ("adidas+nike", 1.0)
      }

      else if (tweet contains "adidas") {
        println("adidas")
        return ("adidas", 1.0)
      }

      else if (tweet contains "nike") {
        println("nike")
        return ("nike", 1.0)
      }
      else {
        return ("not-nike-or-adidas", 0)
      }

    }
    else {
      return ("nike", 0)
    }
  }


  def main(args: Array[String]) {

    //Turn off log info
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    if (args.length < 4) {
      System.err.println(
        s"""
           |Usage: SparkStreaming <brokers> <topics>
           |  <brokers> is a list of one or more Kafka brokers
           |  <topics> is a list of one or more kafka topics to consume from
           |  <windowTime>
           |  <slideWindow>
            """.stripMargin)
      System.exit(1)
    }


    val Array(brokers, topics, windowTime, slideWindow) = args
    val sparkConf = new SparkConf().setAppName("Inc")

    //Tuning performance of Spark
    sparkConf.set("spark.io.compression.codec", "lzf") //compress to improve shuffle performance
    sparkConf.set("spark.speculation", "true") //Turn of speculative execution to present stragglers
    //sparkConf.set("spark.eventLog.enabled","true")

    val ssc = new StreamingContext(sparkConf, Seconds(1))
    ssc.checkpoint("checkpoint")

    // Create direct kafka stream with brokers and topics
    val topicsSet = topics.split(",").toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> brokers,
      "spark.streaming.kafka.maxRatePerPartition" -> "10000", "auto.offset.reset" -> "smallest", "fetch.message.max.bytes" -> "314572800") //30MB
    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](
        ssc, kafkaParams, topicsSet)
    println("Connection is done ####################")

    //val sample_messages = messages.transform(x => x.sample(false, 0.6))
    val sample_lines = messages.map(_._2).flatMap(_.split("########")).cache()
    //val lines = messages.map(_._2).flatMap(_.split("########"))//.cache()

    //process messages receive from Kafka
    val sample_protocol_count = sample_lines.map(x => getHashtagStatistic(x))
      .reduceByKeyAndWindow((a: Double, b: Double) => a + b, (a: Double, b: Double) => a - b, Seconds(windowTime.toInt), Seconds(slideWindow.toInt))

    //val protocol_count = lines.map(x => getHashtagStatistic(x))
    //   .reduceByKeyAndWindow((a:Double, b:Double)=>a+b, Seconds(10), Seconds(2))

    sample_protocol_count.print()
    //protocol_count.print()

    //sample_messages.count().print()
    println("After Sampling: #############")

    ssc.start()
    ssc.awaitTermination()
  }
}
